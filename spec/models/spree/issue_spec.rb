require 'spec_helper'

describe Spree::Issue do
  it "should be part of a samplebox" do
    issue = Factory.build(:issue)
    issue.should respond_to(:samplebox)
  end

  it "should have many shipped issues" do
    issue = Factory.build(:issue)
    issue.should respond_to(:shipped_issues)
  end

  it "should be related to a product which is the samplebox" do
    issue = Factory.create(:issue)
    issue.samplebox.should be_an_instance_of Spree::Product
  end

  it "should not be valid if no samplebox issue and no name is specified" do
    issue = Factory.build(:issue, :name => "", :samplebox_issue => nil)
    issue.should_not be_valid
  end

  it "should have a name like the name of the samplebox issue" do
    issue = Factory.create(:issue, :samplebox_issue => Factory.create(:simple_product))
    issue.name.should equal(issue.samplebox_issue.name)
  end

  it "should have the name attribute if no samplebox issue is present" do
    issue = Factory.create(:issue, :name => "New Issue")
    issue.name.should == "New Issue"
  end

  it "should create a shipped issue when shipping issue" do
    subscription = Factory.create(:paid_subscription)
    issue = Factory.create(:issue, :samplebox => subscription.samplebox)
    expect{ issue.ship! }.to change(issue.shipped_issues, :count).by(1)
  end

  it "should have shipped_at field to nil when not shipped" do
    subscription = Factory.create(:paid_subscription)
    issue = Factory.create(:issue, :samplebox => subscription.samplebox)
    issue.shipped_at.should be_nil
  end

  it "should have shipped_at field not nil when shipped" do
    subscription = Factory.create(:paid_subscription)
    issue = Factory.create(:issue, :samplebox => subscription.samplebox)
    expect{ issue.ship! }.to change{issue.shipped_at}  
  end 
end
