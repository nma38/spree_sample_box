class Spree::Issue < ActiveRecord::Base
  
  # reverse one-to-one with samplebox Spree::Product
  belongs_to :samplebox, :class_name => "Spree::Product"
  belongs_to :samplebox_issue, :class_name => "Spree::Product"

  # composed of shipped # of issues and the content sample products
  has_many :shipped_issues

  # TODO: keep track of how this works out
  # has_many :sample_product, :class_name => "Spree:Product"

  attr_accessible :name, :published_at, :shipped_at, :samplebox, :samplebox_issue_id
  
  # expose the implementation of :subscription, but define it in :samplebox
  delegate :subscriptions,:to => :samplebox

  # TODO: figure out what this is doing
  validates :name, 
    :presence => true,
    :unless => "samplebox_issue.present?"

  def name
    samplebox_issue.present? ? samplebox_issue.name : read_attribute(:name)
  end
  
  # read eligible_for_shipping scope of each subscription.
  # ship! them. 
  # then update all the shipping dates to time of this function call
  def ship!
    subscriptions.eligible_for_shipping.each{ |s| s.ship!(self) }
    update_attribute(:shipped_at, Time.now)
  end

  def shipped?
    !shipped_at.nil?
  end

end
