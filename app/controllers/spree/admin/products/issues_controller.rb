module Spree
  module Admin
    module Products
      class IssuesController < Spree::Admin::BaseController
        before_filter :load_samplebox
        before_filter :load_issue, :only => [:show, :edit, :update, :ship]
        before_filter :load_product, :except => [:show, :index]

        def show
          if @issue.shipped?
            @product_subscriptions = @issue.shipped_issues.map { |shipped_issue| shipped_issue.subscription }.compact
          else
            @product_subscriptions = Subscription.eligible_for_shipping.where(:samplebox_id => @samplebox.id)
          end
          respond_to do |format|
            format.html
            format.pdf do
              addresses_list = @product_subscriptions.map { |s| s.ship_address }
              labels = IssuePdf.new(addresses_list, view_context)
              send_data labels.document.render, :filename => "#{@issue.name}.pdf", :type => "application/pdf", disposition: "inline"
            end
          end
        end

        def index
          @issues = Issue.where(:samplebox_id => @samplebox.id)
        end

        def update
          if @issue.update_attributes(params[:issue])
            flash[:notice] = t('issue_updated')
            redirect_to admin_samplebox_issue_path(@samplebox, @issue)
          else
            flash[:error] = t(:issue_not_updated)
            render :action => :edit
          end
        end

        def new
          @issue = @samplebox.issues.build
        end

        def create
          if (new_issue = @samplebox.issues.create(params[:issue]))
            flash[:notice] = t('issue_created')
            redirect_to admin_samplebox_issue_path(@samplebox, new_issue)
          else
            flash[:error] = t(:issue_not_created)
            render :new
          end
        end

        def ship
          if @issue.shipped?
            flash[:error]  = t('issue_not_shipped')
          else
            @issue.ship!
            flash[:notice]  = t('issue_shipped')
          end
          redirect_to admin_samplebox_issues_path(@samplebox, @issue)
        end

        private

        def load_samplebox
          @samplebox = Product.find_by_permalink(params[:samplebox_id])
          @product = @samplebox # useful to display product_tab menu
        end

        def load_issue
          @issue = Issue.find(params[:id])
        end

        def load_product
          @products = Product.unsubscribable.map { |product| [product.name, product.id] }
        end
      end
    end
  end
end

