Spree::UsersController.class_eval do 
  # extend the user controller with a new method
  # load data in that before calling :show method
  before_filter :include_user_subscriptions, :only => :show

  private

  # load data function
  def include_user_subscriptions
    @subscriptions = Spree::Subscription.where(:email => @user.email)
  end
end
