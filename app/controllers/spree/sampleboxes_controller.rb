module Spree
  class SampleboxesController < Spree::StoreController
    before_filter :load_product, :only => :show
    rescue_from ActiveRecord::RecordNotFound, :with => :render_404
    helper 'spree/products'

    respond_to :html

    def index
      @searcher = Config.searcher_class.new(params)
      @searcher.current_user = try_spree_current_user
      @searcher.current_currency = current_currency
      @products = @searcher.retrieve_products.select { |product| product.subscribable?}
    end

    private
    def accurate_title
      @product ? @product.name : super
    end

    def load_product
      if try_spree_current_user.try(:has_spree_role?, "admin")
        @product = Product.find_by_permalink!(params[:id])
      else
        @product = Product.active(current_currency).find_by_permalink!(params[:id])
      end
    end 
  end
end
