Spree::ProductsController.class_eval do
  # extend the products controller so that index only returns
  # products without subscriptions
   
  def index
    @searcher = Spree::Config.searcher_class.new(params)
    @searcher.current_user = try_spree_current_user
    @searcher.current_currency = current_currency
    @products = @searcher.retrieve_products.reject { |product| product.subscribable? }
    
    # logging in rails
    #Rails.logger.debug("debug::" + @products.inspect)
    #puts @products.inspect
    
    # does not work after setting @variable.
    #@products.reject { |product| product.subscribable? } 
  end 


end

