# encoding: UTF-8
Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'spree_sample_box'
  s.version     = '1.3.0'
  s.summary     = 'Adds subcription based sample boxes to spree.'
  s.description = 'Seperate engine to handle new product types, so updating spree core is easier.'
  s.required_ruby_version = '>= 1.9.2'

  s.author    = 'Nick Ma'
  # s.email     = 'you@example.com'
  # s.homepage  = 'http://www.spreecommerce.com'

  s.files       = `git ls-files`.split("\n")
  s.test_files  = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.require_path = 'lib'
  s.requirements << 'none'

  s.add_dependency 'spree_core', '~> 1.3'
  s.add_dependency 'spree_auth_devise'
  s.add_dependency 'prawn', '~> 0.12.0'
  s.add_dependency 'prawn-labels', '~> 0.11.3'

  s.add_development_dependency 'capybara', '~> 1.1.2'
  s.add_development_dependency 'factory_girl', '~> 2.6.4'
  s.add_development_dependency 'rspec-rails',  '~> 2.9'
  s.add_development_dependency 'factory_girl_rails', '~> 1.7.0'
  s.add_development_dependency 'ffaker'
  s.add_development_dependency 'sass-rails'
  s.add_development_dependency 'sqlite3'
  s.add_development_dependency 'coffee-rails'
end
