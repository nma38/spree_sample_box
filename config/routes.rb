Spree::Core::Engine.routes.draw do
  namespace :admin do
    resources :subscriptions do
      resource :customer, :controller => "subscriptions/customer_details"
    end
    resources :products, :as => :samplebox do
      resources :issues, :controller => "products/issues"
      match "issues/:id/ship", :to => "products/issues#ship", :via => :get, :as => :issue_ship
    end
    resources :users # Hack to make tests pass
  end
  resources :sampleboxes, :to => "sampleboxes#index"
end
