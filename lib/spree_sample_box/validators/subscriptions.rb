class SubscriptionValidator < ActiveModel::Validator 
  def validate(record) 
    unless Spree::Product.find_by_id(record.samplebox_id).subscribable?
      record.errors[:samplebox] << 'Should be a subscribable product'
    end
  end
end
