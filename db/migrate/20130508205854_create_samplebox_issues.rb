class CreateSampleboxIssues < ActiveRecord::Migration
  def change
    create_table :spree_issues do |t|
      t.references :samplebox
      t.references :samplebox_issue
      t.string :name
      t.date :published_at
      t.date :shipped_at

      t.timestamps
    end
    add_index :spree_issues, :samplebox_id
    add_index :spree_issues, :samplebox_issue_id
  end
end
