class AddSampleToProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :is_sample, :boolean, :default => false
  end
end
